module.exports = {
	entry: {
		path: "./src/client.js"
	},
	output: {
		path: __dirname + "	/public",
		filename: "index.js"
	},
	devServer: {
		// inline: true,
		contentBase: "./public",
		port: 3000
	}
}